package echo

/*
#cgo CFLAGS: -I${SRCDIR}/../clibs/echo/
#cgo LDFLAGS: -L${SRCDIR}/../clibs/echo -lecho
#include <echo.h>
*/
import "C"

func Echo(str string) {
	C.echo(C.CString(str))
}
