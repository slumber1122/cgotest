run: clean buildlib gobuild

buildlib:
	gcc -c -o clibs/echo/echo.o clibs/echo/echo.c
	#ar -rv clibs/echo/libecho.a clibs/echo/echo.o
	cc -dynamiclib -o clibs/echo/libecho.dylib clibs/echo/echo.o

gobuild:
	go build -o main main.go

clean:
	rm -f clibs/echo/echo.o clibs/echo/libecho.a
	rm -f main
